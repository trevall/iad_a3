/*
	Reliability and Flow Control Example
	From "Networking for Game Programmers" - http://www.gaffer.org/networking-for-game-programmers
	Author: Glenn Fiedler <gaffer@gaffer.org>
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "Net.h"
#include "md5.h"

//#define SHOW_ACKS

#define MAX_PACKET_SIZE 60000
#define ACK_SIZE 60000

using namespace std;
using namespace net;

const int ServerPort = 30000;
const int ClientPort = 30001;
const int ProtocolId = 0x11223344;
const float DeltaTime = 1.0f / 30.0f;
const float SendRate = 1.0f / 30.0f;
const float TimeOut = 10.0f;
const int PacketSize = 256;

#pragma warning(disable:4996)

class FlowControl
{
public:
	
	FlowControl()
	{
		printf( "flow control initialized\n" );
		Reset();
	}
	
	void Reset()
	{
		mode = Bad;
		penalty_time = 4.0f;
		good_conditions_time = 0.0f;
		penalty_reduction_accumulator = 0.0f;
	}
	
	void Update( float deltaTime, float rtt )
	{
		const float RTT_Threshold = 250.0f;

		if ( mode == Good )
		{
			if ( rtt > RTT_Threshold )
			{
				printf( "*** dropping to bad mode ***\n" );
				mode = Bad;
				if ( good_conditions_time < 10.0f && penalty_time < 60.0f )
				{
					penalty_time *= 2.0f;
					if ( penalty_time > 60.0f )
						penalty_time = 60.0f;
					printf( "penalty time increased to %.1f\n", penalty_time );
				}
				good_conditions_time = 0.0f;
				penalty_reduction_accumulator = 0.0f;
				return;
			}
			
			good_conditions_time += deltaTime;
			penalty_reduction_accumulator += deltaTime;
			
			if ( penalty_reduction_accumulator > 10.0f && penalty_time > 1.0f )
			{
				penalty_time /= 2.0f;
				if ( penalty_time < 1.0f )
					penalty_time = 1.0f;
				printf( "penalty time reduced to %.1f\n", penalty_time );
				penalty_reduction_accumulator = 0.0f;
			}
		}
		
		if ( mode == Bad )
		{
			if ( rtt <= RTT_Threshold )
				good_conditions_time += deltaTime;
			else
				good_conditions_time = 0.0f;
				
			if ( good_conditions_time > penalty_time )
			{
				printf( "*** upgrading to good mode ***\n" );
				good_conditions_time = 0.0f;
				penalty_reduction_accumulator = 0.0f;
				mode = Good;
				return;
			}
		}
	}
	
	float GetSendRate()
	{
		return mode == Good ? 30.0f : 10.0f;
	}
	
private:

	enum Mode
	{
		Good,
		Bad
	};

	Mode mode;
	float penalty_time;
	float good_conditions_time;
	float penalty_reduction_accumulator;
};

struct PacketContainer {
	int numPackets;
	char fileName[100];
	unsigned char buffer[MAX_PACKET_SIZE];
	unsigned char md5Hash[31];
};

// ----------------------------------------------

int main( int argc, char * argv[] )
{
	// parse command line

	enum Mode
	{
		Client,
		Server
	};

	Mode mode = Server;
	Address address;
	ifstream buff;
	vector<unsigned char> buffer;
	vector<PacketContainer> totalBuffer;

	

	int currentSentIndex = 0;

	if ( argc >= 3 )
	{
		int a,b,c,d;
		if ( sscanf( argv[1], "%d.%d.%d.%d", &a, &b, &c, &d ) )
		{
			mode = Client;
			address = Address(a,b,c,d,ServerPort);
		}
		buff.open(argv[2], ios::binary);
		buff.seekg(0, std::ios::beg);
		int count = 0;
		char b2[MAX_PACKET_SIZE];
		MD5 md5;
		char* f = md5.digestFile(argv[2]);
		printf("Total Hash: %s", f);
		while (!buff.eof()) 
		{
			buff.read(b2, sizeof(b2));
		
			PacketContainer* pkt = new PacketContainer;
			
			//pkt->buffer = (unsigned char*)malloc(sizeof(b2) * sizeof(char) + 1);
			//pkt->md5Hash = (unsigned char*)malloc(strlen(f) * sizeof(char) + 1);
			
			f = md5.digestMemory(reinterpret_cast<unsigned char *>(b2), sizeof(b2) + 1);

			memset(pkt->buffer, 0, sizeof(pkt->buffer));
			memset(pkt->md5Hash, 0, sizeof(pkt->md5Hash));
			memset(pkt->fileName, 0, sizeof(pkt->fileName));

			memcpy(pkt->md5Hash, f, strlen(f));
			pkt->md5Hash[30] = '\0';
			memcpy(pkt->buffer, b2, sizeof(b2) * sizeof(char));
			
			strcpy(pkt->fileName, argv[2]);
			//pkt->fileName = argv[2];
			
			pkt->numPackets = totalBuffer.size();

			totalBuffer.push_back(*pkt);

		}
		totalBuffer.back().numPackets = -1;
	}

	// initialize

	if ( !InitializeSockets() )
	{
		printf( "failed to initialize sockets\n" );
		return 1;
	}

	ReliableConnection connection( ProtocolId, TimeOut );
	
	const int port = mode == Server ? ServerPort : ClientPort;
	
	if ( !connection.Start( port ) )
	{
		printf( "could not start connection on port %d\n", port );
		return 1;
	}
	
	if ( mode == Client )
		connection.Connect( address );
	else
		connection.Listen();
	
	bool connected = false;
	float sendAccumulator = 0.0f;
	float statsAccumulator = 0.0f;
	
	FlowControl flowControl;
	
	while ( true )
	{
		// update flow control
		
		if ( connection.IsConnected() )
			flowControl.Update( DeltaTime, connection.GetReliabilitySystem().GetRoundTripTime() * 1000.0f );
		
		const float sendRate = flowControl.GetSendRate();

		// detect changes in connection state

		if ( mode == Server && connected && !connection.IsConnected() )
		{
			flowControl.Reset();
			printf( "reset flow control\n" );
			connected = false;
		}

		if ( !connected && connection.IsConnected() )
		{
			printf( "client connected to server\n" );
			connected = true;
		}
		
		if ( !connected && connection.ConnectFailed() )
		{
			printf( "connection failed\n" );
			break;
		}
		
		// send and receive packets
		
		sendAccumulator += DeltaTime;

		if (mode == Client)
		{			
			PacketContainer *pkt = new PacketContainer;
			if (totalBuffer.size() > currentSentIndex)
			{
				pkt = &totalBuffer.at(currentSentIndex);
			}
			else
			{
				break;
			}
			//memset(pkt->buffer, 0, sizeof(pkt->buffer));
			//memset(pkt->md5Hash, 0, sizeof(pkt->md5Hash));
			//memset(pkt->fileName, 0, sizeof(pkt->fileName));
			//strcpy(pkt->fileName, "DEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEFDEADBEEF");
			unsigned char packet[sizeof(PacketContainer)];
			memmove(&packet, reinterpret_cast<unsigned char*>(pkt), sizeof(PacketContainer));
			connection.SendPacket(packet, sizeof(packet));
			sendAccumulator -= 1.0f / sendRate;
			currentSentIndex++;
		}
		else if (mode == Server)
		{
			unsigned char packet[ACK_SIZE];
			memset(packet, 0, ACK_SIZE);
			connection.SendPacket(packet, sizeof(packet));
			sendAccumulator -= 1.0f / sendRate;
		}


		if (mode == Client)
		{
			while (true)
			{
				unsigned char packet[ACK_SIZE];
				int bytes_read = connection.ReceivePacket(packet, sizeof(packet));
				if (bytes_read == 0)
					break;
			}
		}
		else if (mode == Server)
		{
			while (true)
			{
				PacketContainer *pkt = new PacketContainer;
				unsigned char packet[sizeof(PacketContainer)];
				int bytes_read = connection.ReceivePacket(packet, sizeof(packet));
				if (bytes_read == 0)
					break;
				else
				{
					pkt = reinterpret_cast<PacketContainer*>(packet);
					printf("%d\n", pkt->numPackets);
					if (pkt->numPackets == -1)
					{
						break;
					}
				}
			}
		}


		//while (sendAccumulator > 1.0f / sendRate)
		//{
		//	PacketContainer *pkt = new PacketContainer;
		//	if (totalBuffer.size() > 0)
		//	{
		//		unsigned char packet[sizeof(totalBuffer[0])];
		//		memcpy(packet, &totalBuffer[0], sizeof(totalBuffer[0]));
		//		//memset(packet, 0, sizeof(packet));
		//		connection.SendPacket(packet, sizeof(packet));
		//	}
		//	else
		//	{
		//		PacketContainer* pkt = new PacketContainer;
		//	}
		//	sendAccumulator -= 1.0f / sendRate;
		//}
		//
		//while ( true )
		//{
		//	unsigned char packet[sizeof(PacketContainer)];
		//	int bytes_read = connection.ReceivePacket( packet, sizeof(packet) );
		//	PacketContainer* pkt = new PacketContainer;
		//	memcpy(pkt, packet, sizeof(PacketContainer));			
		//	if ( bytes_read == 0 )
		//		break;
		//	else
		//		printf("%s\n", pkt->md5Hash);
		//}
		
		// show packets that were acked this frame
		
		#ifdef SHOW_ACKS
		unsigned int * acks = NULL;
		int ack_count = 0;
		connection.GetReliabilitySystem().GetAcks( &acks, ack_count );
		if ( ack_count > 0 )
		{
			printf( "acks: %d", acks[0] );
			for ( int i = 1; i < ack_count; ++i )
				printf( ",%d", acks[i] );
			printf( "\n" );
		}
		#endif

		// update connection
		
		connection.Update( DeltaTime );

		// show connection stats
		
		statsAccumulator += DeltaTime;

		while ( statsAccumulator >= 0.25f && connection.IsConnected() )
		{
			float rtt = connection.GetReliabilitySystem().GetRoundTripTime();
			
			unsigned int sent_packets = connection.GetReliabilitySystem().GetSentPackets();
			unsigned int acked_packets = connection.GetReliabilitySystem().GetAckedPackets();
			unsigned int lost_packets = connection.GetReliabilitySystem().GetLostPackets();
			
			float sent_bandwidth = connection.GetReliabilitySystem().GetSentBandwidth();
			float acked_bandwidth = connection.GetReliabilitySystem().GetAckedBandwidth();
			
			printf( "rtt %.1fms, sent %d, acked %d, lost %d (%.1f%%), sent bandwidth = %.1fkbps, acked bandwidth = %.1fkbps\n", 
				rtt * 1000.0f, sent_packets, acked_packets, lost_packets, 
				sent_packets > 0.0f ? (float) lost_packets / (float) sent_packets * 100.0f : 0.0f, 
				sent_bandwidth, acked_bandwidth );
			
			statsAccumulator -= 0.25f;
		}

		net::wait( DeltaTime );
	}
	
	ShutdownSockets();

	return 0;
}
